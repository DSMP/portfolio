import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home/Home';
import { Solutions } from './components/Solutions';
import { Bussiness } from './components/Bussiness';
import { Blog } from './components/Blog';
import { Projects } from './components/Projects';
import { Contact } from './components/Contact';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/Solutions' component={Solutions} />
                <Route path='/Bussiness' component={Bussiness} />
                <Route path='/Blog' component={Blog} />
                <Route path='/Projects' component={Projects} />
                <Route path='/Contact' component={Contact} />
            </Layout>
        );
    }
}
