import React, { Component } from 'react';
import { PageComponent } from './PageComp';

export class Projects extends Component {
    render() {
        return (
            <PageComponent Style="fifth" Header="Projects" Api="Projects"
                Description="Here it is my portfolio. It shows my finished projects." />
        )
    }
}