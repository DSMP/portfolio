﻿import React, { Component } from 'react';
import { HomeContainer } from './Home/HomeContainer';
import { ItemList } from "./ItemList"

export class PageComponent extends Component {
    render() {
        return (
            <div>
                <HomeContainer Style={this.props.Style} Header={this.props.Header} Uri={this.props.Uri}
                    Description={this.props.Description} />
                <ItemList Api={this.props.Api}/>
            </div>
        )
    }
}