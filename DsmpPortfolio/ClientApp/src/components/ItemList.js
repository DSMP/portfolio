﻿import React, { Component } from 'react';
import { Item } from './Item';
import './ItemList.css';
var images = require.context('./../Resources', true);

export class ItemList extends Component {
    constructor(props) {
        super(props);
        this.state = { projects: [], isLoading: true };
    }

    componentDidMount() {
        this.populateProjectsData();
    }

    static renderItems(newProjects) {
        let imgOnRight = false;
        return (
            <div className="Items">
                {newProjects.map(p =>
                    <Item
                        img={p.imageLink.startsWith(`http`) ? p.imageLink : images(`./${p.imageLink}`)}
                        Name={p.itemName}
                        paragraph={p.text}
                        isRightImg={imgOnRight = !imgOnRight} />
                )}
            </div>
        );
    }

    render() {
        let contents = this.state.isLoading
            ? <p><em>Loading projects...</em></p>
            : ItemList.renderItems(this.state.projects);

        return (
            <div>
                {contents}
            </div>
        )
    }
    async populateProjectsData() {
        const response = await fetch('api/PageDetails/' + this.props.Api);
        const data = await response.json();
        this.setState({ projects: data, isLoading: false });
    }
}