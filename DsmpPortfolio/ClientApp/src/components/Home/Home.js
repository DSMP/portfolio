import React, { Component } from 'react';
import { HomeContainer } from './HomeContainer';
import './Home.css';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <div class="container-fluid first container-padding">
                    <h1 class="display-4 text-monospace text-center main-font">
                        Looking for solution for your company?
                    </h1>
                    <div class="row text-center myrow">
                        <div class="col">
                            <button type="button" class="btn btn-outline-primary btn-size">Solutions</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-outline-primary btn-size">Blog</button>    
                        </div>
                    </div>
                    <div class="row text-center myrow">
                        <div class="col">
                            <button type="button" class="btn btn-outline-primary btn-size">Bussiness</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-outline-primary btn-size">Projects</button>
                        </div>
                    </div>
                </div>
                <HomeContainer Style="second" Header="Mobile App Development" Uri="/Contact"
                    Description="Provide applications on all platforms. Use mobile possibilities in your company. Make your application on all platforms today." />
                <HomeContainer Style="third" Header="Web App Development" Uri="/Contact"
                    Description="Web application gives your company be shown from Internet. Web-site can provide a lot of possibility to managing your company or provide information to your customers. Make your Web app today." />
                <HomeContainer Style="fourth" Header="Desktop" Uri="/Contact"
                    Description="Desktop application can provide intern managing of your company or make your job simplier. Make your application on your desktop today." />
                <HomeContainer Style="fifth" Header="Custom solutions" Uri="/Contact"
                    Description="Your company need to integrate with other companies? Maybe your company nned to integrate with your selfs services? Get more information here." />
            </div>
        );
    }
}
