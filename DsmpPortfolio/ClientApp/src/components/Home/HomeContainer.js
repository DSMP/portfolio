﻿import React, { Component } from 'react';
import './HomeContainer.css';

export class HomeContainer extends Component {
    render() {
        let styleName = "container-fluid container-height d-flex align-content-center flex-wrap justify-content-center " + this.props.Style;
        return (
            <div class={styleName}>
                <div class="text-padding">
                    <div className="animFromTop">
                        <h1 class="display-3 text-monospace text-center">
                            {this.props.Header}
                        </h1>
                    </div>
                    <p>{this.props.Description}</p>
                    {this.props.Uri != undefined &&
                        < a href={this.props.Uri}>Contact with me...</a>
                    }
                </div>
            </div >
        )
    }
}