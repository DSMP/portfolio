﻿import React, { Component } from 'react';
import './Contact.css'

export class Contact extends Component {
    constructor() {
        super();
        this.state = { ClientName: '', ClientEmail: '', Description: '' };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleDescChange = this.handleDescChange.bind(this);
        this.CanSend = true;
    }
    handleNameChange(e) {
        this.setState({ ClientName: e.target.value });
    }
    handleEmailChange(e) {
        let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        let email = e.target.value;
        if (re.test(String(email).toLowerCase())) {
            this.setState({ ClientEmail: e.target.value });
            this.CanSend = true;
        } else {
            this.CanSend = false;
        }
    }
    handleDescChange(e) {
        this.setState({ Description: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.CanSend) {
            alert("You fill wrong E-mail address");
            return;
        }
        const data = new FormData();
        data.append('ClientName', this.state.ClientName);
        data.append('ClientEmail', this.state.ClientEmail);
        data.append('Description', this.state.Description);
        var xhr = new XMLHttpRequest();
        xhr.open('post', 'api/Contact', true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status == 200 || xhr.status == 201)) {
                // Do something on success
                alert('Contact sent');
                document.getElementById('contactForm').reset();
            }
        }
        xhr.send(data);
    }

    render() {
        return (
            <div className="container d-flex flex-column align-items-center contactStyle">
                <div className="d-flex align-content-center justify-content-center">
                    My name is Damian. I am living in Northern Ireland.
                </div>
                <div className="d-flex align-content-center justify-content-center">
                    If you want to Contact with me, please fill form below.
                </div>
                <div className="d-flex align-content-center justify-content-center formStyle">
                    <form id="contactForm" onSubmit={this.handleSubmit}>
                        <fieldset>
                            <legend>Contact with me:</legend>
                            <p className="formText">Fill your name: </p>
                            <input className="d-flex align-self-stretch inputSpacing" type="text" name="ClientName" value={this.state.clientName}
                                onChange={this.handleNameChange} autofocus required />
                            <p className="formText">Fill your E-mail: </p>
                            <input className="d-flex align-self-stretch inputSpacing" type="text" name="ClientEmail" value={this.state.clientEmail}
                                onChange={this.handleEmailChange} required />
                            <p className="formText">Please describe your idea below: </p>
                            <textarea className="contactDesc" name="Description" value={this.state.description}
                                onChange={this.handleDescChange} required /><br />
                            <input className="contactSubmit" type="submit" value="Submit" onclick="alert('Hello World!')" />
                        </fieldset>
                    </form>
                </div>

            </div>
        )
    }
}