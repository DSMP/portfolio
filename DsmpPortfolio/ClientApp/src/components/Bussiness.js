﻿import React, { Component } from 'react';
import { PageComponent } from './PageComp';

export class Bussiness extends Component {
    render() {
        return (
            <PageComponent Style="third" Header="Bussiness" Api="Bussiness"
                Description="Make your bussiness running easier. Provide solution which make possibilities to grow your bussines faster or just to make your employees job easier. Find new clients using e-comerce solutions today."/>
        )
    }
}