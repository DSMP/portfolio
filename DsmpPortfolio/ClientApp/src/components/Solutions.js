﻿import React, { Component } from 'react';
import { PageComponent } from './PageComp';

export class Solutions extends Component {
    render() {
        return (
            <PageComponent Style="second" Header="Custom solutions" Api="Solutions"
                Description="Your company need to integrate with other companies? Your solution is not working well? Get more information here."/>
        )
    }
}