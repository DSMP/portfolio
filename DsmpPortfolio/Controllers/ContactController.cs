﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DsmpPortfolio.Models.Db;
using DsmpPortfolio.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using DsmpPortfolio.Models.Dto;
using DsmpPortfolio.Repositories;

namespace DsmpPortfolio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly ContactRepository _contactRepository;
        private readonly PortfolioContext _context;

        public ContactController(ContactRepository contactRepository,PortfolioContext context)
        {
            this._contactRepository = contactRepository;
            _context = context;
        }

        // GET: api/Contact
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContactForm>>> GetContactForm()
        {
            return await _context.ContactForm.ToListAsync();
        }

        // GET: api/Contact/5
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ContactForm>> GetContactForm(int id)
        {
            var contactForm = await _context.ContactForm.FindAsync(id);

            if (contactForm == null)
            {
                return NotFound();
            }

            return contactForm;
        }

        // PUT: api/Contact/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContactForm(int id, ContactForm contactForm)
        {
            if (id != contactForm.ContactId)
            {
                return BadRequest();
            }

            _context.Entry(contactForm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactFormExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contact
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ContactForm>> PostContactForm([FromForm]ContactDto contactDto)
        {
            if (contactDto == null || contactDto.ClientEmail.Equals("undefined"))
            {
                var res = new ObjectResult("");
                res.StatusCode = StatusCodes.Status204NoContent;
                return res;
            }
            await _contactRepository.SaveContact(contactDto);

            return new CreatedResult("/Contact","Created");
        }

        // DELETE: api/Contact/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<ContactForm>> DeleteContactForm(int id)
        {
            var contactForm = await _context.ContactForm.FindAsync(id);
            if (contactForm == null)
            {
                return NotFound();
            }

            _context.ContactForm.Remove(contactForm);
            await _context.SaveChangesAsync();

            return contactForm;
        }

        private bool ContactFormExists(int id)
        {
            return _context.ContactForm.Any(e => e.ContactId == id);
        }
    }
}
