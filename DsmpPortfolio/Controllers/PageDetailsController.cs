﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DsmpPortfolio.Models.Db;
using DsmpPortfolio.Models.Entities;
using Microsoft.AspNetCore.Authorization;

namespace DsmpPortfolio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PageDetailsController : ControllerBase
    {
        private readonly PortfolioContext _context;

        public PageDetailsController(PortfolioContext context)
        {
            _context = context;
        }

        // GET: api/PageDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PageDetails>>> GetPageDetails()
        {
            return await _context.PageDetails.ToListAsync();
        }

        // GET: api/PageDetails/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<PageDetails>> GetPageDetails(int id)
        {
            var pageDetails = await _context.PageDetails.FindAsync(id);

            if (pageDetails == null)
            {
                return NotFound();
            }

            return pageDetails;
        }
        [HttpGet("{pageName}")]
        public ActionResult<List<PageItem>> GetPageDetailsByName(string pageName)
        {
            var pagesDetails = _context.PageDetails.Where(p => p.PageName.Equals(pageName)).Select(s => s.Items).FirstOrDefault();

            if (pagesDetails == null)
            {
                return NotFound();
            }

            return pagesDetails.ToList();
        }

        // PUT: api/PageDetails/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPageDetails(int id, PageDetails pageDetails)
        {
            if (id != pageDetails.PageId)
            {
                return BadRequest();
            }

            _context.Entry(pageDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PageDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PageDetails
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<PageDetails>> PostPageDetails(PageDetails pageDetails)
        {
            _context.PageDetails.Add(pageDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPageDetails", new { id = pageDetails.PageId }, pageDetails);
        }

        // DELETE: api/PageDetails/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<PageDetails>> DeletePageDetails(int id)
        {
            var pageDetails = await _context.PageDetails.FindAsync(id);
            if (pageDetails == null)
            {
                return NotFound();
            }

            _context.PageDetails.Remove(pageDetails);
            await _context.SaveChangesAsync();

            return pageDetails;
        }

        private bool PageDetailsExists(int id)
        {
            return _context.PageDetails.Any(e => e.PageId == id);
        }
    }
}
