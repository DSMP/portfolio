using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DsmpPortfolio.Models.Db;
using DsmpPortfolio.Models.Entities;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DsmpPortfolio
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            //_InitDb();
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        
        private static void _InitDb()
        {
            var builderOptions = new DbContextOptionsBuilder();
            builderOptions.UseNpgsql(Startup.Settings.ConnectionStrings.DefaultConnection);
            using (var ctx = new PortfolioContext(builderOptions.Options))
            {
                ctx.PageDetails.Add(new PageDetails { PageName = "Solutions", Items = new List<PageItem> {  } });
                var item1 = new PageItem
                {
                    ItemName = "Restaurant solution",
                    Text = "Using web application and mobile applications it is possibly to make solution for client's restaurants to: show offer, order, make food delivery and make resevation in the restaurant. To make better performance for restaurant stuff, it is good to get hand printer to provide order to the kitchen.",
                    ImageLink = "https://organicthemes.com/demo/restaurant/wp-content/themes/organic-restaurant/images/logo.png"
                };
                var item2 = new PageItem
                {
                    ItemName = "Simple application",
                    Text = "Using mobile device, You can show your products or configure/make calculation with your client on the meeting",
                    ImageLink = "https://mir-s3-cdn-cf.behance.net/projects/max_808/11805375.5481e87354eb9.png"
                };
                ctx.ItemEntities.Add(item1);
                ctx.ItemEntities.Add(item2);
                var item3 = new PageItem
                {
                    ItemName = "Simple online shop",
                    Text = "Using web application it is possibly to provide online store for your customer. That application not only show products for customers and allow to put them to the basket. That provide for you statistics of your store and managment panel for admin of your online store",
                    ImageLink = "https://www.ovh.co.uk/images/hosting2016/panel-administration-prestashop-ovh-EN.jpg"
                };
                var item4 = new PageItem
                {
                    ItemName = "Web visiting site",
                    Text = "Provide simple web application for present your self on it.",
                    ImageLink = "https://unblast.com/wp-content/uploads/2018/11/Business-Card-Mockup-Vol-03.jpg"
                };
                ctx.ItemEntities.Add(item3);
                ctx.ItemEntities.Add(item4);
                ctx.PageDetails.Add(new PageDetails { PageName = "Bussiness", Items = new List<PageItem> { item1, item2, item3, item4 } });
                var project1 = new PageItem
                {
                    ItemName = "Mobile restaurant application",
                    Text = "Application provide information about restaurant offer, and location of the restaurant with possibility to designate trace to the restaurant using google maps",
                    ImageLink = "SimpleRestaurantApp.png"
                };
                ctx.PageDetails.Add(new PageDetails { PageName = "Projects", Items = new List<PageItem> { project1 } });
                ctx.SaveChanges();
            }
        }
    }
}
