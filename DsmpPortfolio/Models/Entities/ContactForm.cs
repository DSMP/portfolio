﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DsmpPortfolio.Models.Entities
{
    public class ContactForm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactId { get; set; }
        public DateTime DateInserted { get; } = DateTime.UtcNow;
        public bool IsReaded { get; set; }
        [MaxLength(100)]
        public string ClientName { get; set; }
        [MaxLength(100)]
        public string ClientEmail { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
    }
}
