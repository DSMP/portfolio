﻿using DsmpPortfolio.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DsmpPortfolio.Models.Db
{
    public class PortfolioContext : DbContext
    {
        public DbSet<PageItem> ItemEntities { get; set; }
        public DbSet<PageDetails> PageDetails { get; set; }
        public DbSet<ContactForm> ContactForm { get; set; }

        public PortfolioContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // MS SQL current date problem -> default value
            //modelBuilder
            //    .Entity<ContactForm>()
            //    .Property(e => e.DateInserted)
            //    .HasDefaultValueSql("getdate()");
        }


    }
}
