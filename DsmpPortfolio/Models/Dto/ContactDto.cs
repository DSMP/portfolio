﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DsmpPortfolio.Models.Dto
{
    public class ContactDto
    {
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string Description { get; set; }
    }
}
