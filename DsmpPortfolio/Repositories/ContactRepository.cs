﻿using DsmpPortfolio.Models.Db;
using DsmpPortfolio.Models.Dto;
using DsmpPortfolio.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DsmpPortfolio.Repositories
{
    public class ContactRepository
    {
        private readonly PortfolioContext _context;
        public ContactRepository(PortfolioContext context)
        {
            _context = context;
        }
        public async Task SaveContact(ContactDto contactDto)
        {
            try
            {

                var newContactForm = new ContactForm
                {
                    ClientName = contactDto.ClientName,
                    ClientEmail = contactDto.ClientEmail,
                    Description = contactDto.Description,
                    IsReaded = false
                };
                _context.ContactForm.Add(newContactForm);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
